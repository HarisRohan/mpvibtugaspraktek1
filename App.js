/* eslint-disable no-alert */
/* eslint-disable prettier/prettier */
/* eslint-disable no-unused-vars */
/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Text,
} from 'react-native';

class HomePage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      firstName: '',
      lastName: '',
      fullName: '',
    };
  }

  Check = () => {
    alert('Hello, ' + this.state.firstName + ' ' + this.state.lastName);
  }
  render () {
    return (
      <View style={styles.wrapper}>
        <TextInput style={styles.form} placeholder="First Name"
        onChangeText={firstName => this.setState({firstName : firstName})}
        value={this.state.firstName}
        />
        <TextInput style={styles.form} placeholder="Last Name"
        onChangeText={lastName => this.setState({lastName : lastName})}
        value={this.state.lastName}
        />
        <TouchableOpacity
        style={styles.button}
        title="Check"
        onPress={this.Check}>
          <Text
            style={{color: 'white', textAlign: 'center', position: 'absolute'}}>
            CHECK
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default HomePage;

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    padding: 20,
    flexDirection: 'column',
    backgroundColor: '#FFF8D1',
    alignItems: 'baseline',
    justifyContent: 'center',
    position: 'relative',
  },
  form: {
    borderBottomColor: '#ded7b1',
    height: 40,
    width: '100%',
    textAlign: 'left',
    margin: 3,
    paddingLeft: 20,
    borderBottomWidth: 2,
  },
  button: {
    backgroundColor: '#ffb300',
    borderRadius: 100,
    height: 40,
    width: '100%',
    alignItems: 'center',
    marginTop: 25,
    justifyContent: 'center',
    paddingTop: 20,
  },
});
